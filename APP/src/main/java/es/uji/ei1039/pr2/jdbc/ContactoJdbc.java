package es.uji.ei1039.pr2.jdbc;

import org.springframework.dao.EmptyResultDataAccessException;

import es.uji.ei1039.pr2.dao.ContactoDao;
import es.uji.ei1039.pr2.model.Contacto;
import es.uji.ei1039.pr2.rowmapper.ContactoRowMapper;
import es.uji.ei1039.pr2.rowmapper.StringRowMapper;

public class ContactoJdbc extends JdbcTemplateAbs<Contacto> implements ContactoDao {
	
	private static final String TABLE_NAME = "Contacto";
	private static final int NUM_PARAM = 2;
	
	public ContactoJdbc() {
		super(TABLE_NAME, NUM_PARAM, new ContactoRowMapper());
	}
	
	void doAction(String sql, Contacto c){
		jdbcTemplate.update(sql, c.getName(), c.getApellido());
	}

	@Override
	public Contacto get(int id) {
		try{
            return jdbcTemplate.queryForObject("SELECT * FROM Contacto WHERE id=?", new ContactoRowMapper(), id);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
	}

	@Override
	public void delete(Contacto contacto) {
		jdbcTemplate.update("DELETE FROM Contacto WHERE id=?", contacto.getId());
	}

	@Override
	public void delete(int id) {
		jdbcTemplate.update("DELETE FROM Contacto WHERE id=?", id);
	}

	@Override
	public void update(Contacto contacto) {
		jdbcTemplate.update("UPDATE Contacto SET nombre=?, apellido=? WHERE id=?", contacto.getName(), contacto.getApellido(), contacto.getId());
	}

	@Override
	public int addC(Contacto contacto) {
		super.add(contacto);
		return Integer.parseInt(jdbcTemplate.queryForObject("SELECT * FROM Contacto WHERE nombre=? and apellido=?", new StringRowMapper("id"), contacto.getName(), contacto.getApellido()));
	}
	

}


