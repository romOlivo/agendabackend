package es.uji.ei1039.pr2.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.uji.ei1039.pr2.model.Telefono;

public final class TelefonoRowMapper implements MyMapper<Telefono> {
	
	@Override
	public Telefono mapRow(ResultSet data, int arg1) throws SQLException {
		Telefono dev = new Telefono();
		dev.setId(data.getInt("id"));
		dev.setEtiqueta(data.getString("etiqueta"));
		dev.setNumero(data.getInt("numero"));
		return dev;
	}

	@Override
	public MyMapper<Telefono> create() {
		return new TelefonoRowMapper();
	}

}
