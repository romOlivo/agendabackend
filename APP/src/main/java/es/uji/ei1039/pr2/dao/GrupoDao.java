package es.uji.ei1039.pr2.dao;

import java.util.List;

import es.uji.ei1039.pr2.model.Grupo;

public interface GrupoDao {
	
	void add (Grupo grupo);
	Grupo get (String nombre);
	List<Grupo> getAll();
	void delete (String nombre);

}
