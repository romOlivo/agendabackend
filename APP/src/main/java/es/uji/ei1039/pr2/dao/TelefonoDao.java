package es.uji.ei1039.pr2.dao;

import java.util.List;

import es.uji.ei1039.pr2.model.Telefono;

public interface TelefonoDao {
	
	void add(Telefono telefono);
	Telefono get(int numero);
	List<Telefono> getContactPhones(int id);
	void delete(int numero);
	List<Telefono> getAll();
	List<String> getAllEtiquetas();

}
