package es.uji.ei1039.pr2.jdbc;

import org.springframework.dao.EmptyResultDataAccessException;

import es.uji.ei1039.pr2.dao.GrupoDao;
import es.uji.ei1039.pr2.model.Grupo;
import es.uji.ei1039.pr2.rowmapper.GrupoRowMapper;

public class GrupoJdbc extends JdbcTemplateAbs<Grupo> implements GrupoDao {

	private static final String TABLE_NAME = "Grupo";
	private static final int NUM_PARAM = 1;
	
	public GrupoJdbc() {
		super(TABLE_NAME, NUM_PARAM, new GrupoRowMapper());
	}

	@Override
	public Grupo get(String nombre) {
		try{
            return jdbcTemplate.queryForObject("SELECT * FROM Grupo WHERE nombre=?", new GrupoRowMapper(), nombre);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
	}

	@Override
	public void delete(String nombre) {
		jdbcTemplate.update("DELETE FROM Grupo WHERE nombre=?", nombre);
	}

	@Override
	void doAction(String sql, Grupo obj) {
		jdbcTemplate.update(sql, obj.getNombre());
	}
	
}
