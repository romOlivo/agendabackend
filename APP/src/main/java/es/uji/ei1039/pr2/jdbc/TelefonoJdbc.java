package es.uji.ei1039.pr2.jdbc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;

import es.uji.ei1039.pr2.dao.TelefonoDao;
import es.uji.ei1039.pr2.model.Contacto;
import es.uji.ei1039.pr2.model.Telefono;
import es.uji.ei1039.pr2.rowmapper.StringRowMapper;
import es.uji.ei1039.pr2.rowmapper.TelefonoRowMapper;

public class TelefonoJdbc extends JdbcTemplateAbs<Telefono> implements TelefonoDao {

	private static final String TABLE_NAME = "Telefono";
	private static final int NUM_PARAM = 3;
	
	public TelefonoJdbc() {
		super(TABLE_NAME, NUM_PARAM, new TelefonoRowMapper());
	}
	
	void doAction(String sql, Contacto c){
		jdbcTemplate.update(sql, c.getName(), c.getApellido());
	}

	@Override
	public Telefono get(int numero) {
		try{
            return jdbcTemplate.queryForObject("SELECT * FROM Telefono WHERE numero=?", new TelefonoRowMapper(), numero);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
	}

	@Override
	public void delete(int numero) {
		jdbcTemplate.update("DELETE FROM Telefono WHERE numero=?", numero);
	}

	@Override
	public List<Telefono> getContactPhones(int id) {
		try {
            return jdbcTemplate.query("SELECT * FROM Telefono WHERE id=?", rowMapper.create(), id);
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<Telefono>();
        }
	}

	@Override
	void doAction(String sql, Telefono t) {
		jdbcTemplate.update(sql, t.getNumero(), t.getEtiqueta(), t.getId());
	}

	@Override
	public List<String> getAllEtiquetas() {
		try {
            return jdbcTemplate.query("SELECT DISTINCT(etiqueta) FROM Telefono", new StringRowMapper("etiqueta"));
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<String>();
        }
	}
	
}
