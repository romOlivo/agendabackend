package es.uji.ei1039.pr2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.uji.ei1039.pr2.dao.ContGrupoDao;
import es.uji.ei1039.pr2.model.ContGrupo;
import es.uji.ei1039.pr2.model.Contacto;

@RestController
@RequestMapping("/cg")
public class ContGrupoController {
	
	private ContGrupoDao contGrupoDao;
	
	@Autowired
	public void setGrupoDaoo(ContGrupoDao contGrupoDao) {
		this.contGrupoDao = contGrupoDao; 
	}
	
	// Devuelve todas las relaciones (Contacto - Grupo) de la base de datos
	@GetMapping("")
	public List<ContGrupo> getAll(){
		return contGrupoDao.getAll();
	}
	
	// Crea una nueva relación
	@PostMapping("/{id}/{grupo}")
	public String add(@PathVariable int id, @PathVariable String grupo) {
		contGrupoDao.add(id, grupo);
		return "ok";
	}
	
	// Devuelve todos grupos de un contacto
	@GetMapping("/c/{id}")
	public List<ContGrupo> getAllContacto(@PathVariable int id) {
		return contGrupoDao.getAllContacto(id);
	}
	
	// Devuelve todos los contactos de un grupo
	@GetMapping("/g/{grupo}")
	public List<Contacto> getAllGrupo(@PathVariable String grupo) {
		return contGrupoDao.getAllGrupo(grupo);
	}
	
	// Elimina una relación
	@PostMapping("/del/{id}/{grupo}")
	public String delete(@PathVariable int id, @PathVariable String grupo) {
		contGrupoDao.delete(id, grupo);
		return "ok";
	}

}
