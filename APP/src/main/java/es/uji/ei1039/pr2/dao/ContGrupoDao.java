package es.uji.ei1039.pr2.dao;

import java.util.List;

import es.uji.ei1039.pr2.model.ContGrupo;
import es.uji.ei1039.pr2.model.Contacto;

public interface ContGrupoDao {
	
	void add (int id, String nombre);
	void delete(int id, String nombre);
	List<ContGrupo> getAll();
	List<ContGrupo> getAllContacto(int id);
	List<Contacto> getAllGrupo(String grupo);

}
