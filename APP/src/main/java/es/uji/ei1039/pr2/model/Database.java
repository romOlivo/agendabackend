package es.uji.ei1039.pr2.model;

import java.util.ArrayList;
import java.util.List;

public class Database {
	
	private List<Contacto> contactos = new ArrayList<Contacto>();
	private List<ContGrupo> contGrupos = new ArrayList<ContGrupo>();
	private List<Email> emails = new ArrayList<Email>();
	private List<Grupo> grupos = new ArrayList<Grupo>();
	private List<Telefono> telefonos = new ArrayList<Telefono>();
	
	public List<Contacto> getContactos() {
		return contactos;
	}
	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}
	public List<ContGrupo> getContGrupos() {
		return contGrupos;
	}
	public void setContGrupos(List<ContGrupo> contGrupos) {
		this.contGrupos = contGrupos;
	}
	public List<Email> getEmails() {
		return emails;
	}
	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}
	public List<Grupo> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}
	public List<Telefono> getTelefonos() {
		return telefonos;
	}
	public void setTelefonos(List<Telefono> telefonos) {
		this.telefonos = telefonos;
	}
	

}
