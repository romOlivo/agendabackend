package es.uji.ei1039.pr2.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.uji.ei1039.pr2.model.Contacto;

public final class ContactoRowMapper implements MyMapper<Contacto> {

	@Override
	public Contacto mapRow(ResultSet data, int arg1) throws SQLException {
		Contacto dev = new Contacto();
		dev.setId(data.getInt("id"));
		dev.setName(data.getString("nombre"));
		dev.setApellido(data.getString("apellido"));
		return dev;
	}

	@Override
	public MyMapper<Contacto> create() {
		return new ContactoRowMapper();
	}

}


