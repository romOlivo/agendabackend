package es.uji.ei1039.pr2.jdbc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;

import es.uji.ei1039.pr2.dao.EmailDao;
import es.uji.ei1039.pr2.model.Email;
import es.uji.ei1039.pr2.rowmapper.EmailRowMapper;
import es.uji.ei1039.pr2.rowmapper.StringRowMapper;

public class EmailJdbc extends JdbcTemplateAbs<Email> implements EmailDao {
	
	private static final String TABLE_NAME = "Email";
	private static final int NUM_PARAM = 3;

	public EmailJdbc() {
		super(TABLE_NAME, NUM_PARAM, new EmailRowMapper());
	}

	@Override
	public Email get(String correo) {
		try{
            return jdbcTemplate.queryForObject("SELECT * FROM Email WHERE correo=?", new EmailRowMapper(), correo);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
	}

	@Override
	public List<Email> getAllContacto(int id) {
		try {
            return jdbcTemplate.query("SELECT * FROM Email WHERE id=?", new EmailRowMapper(), id);
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<Email>();
        }
	}

	@Override
	public void delete(String correo) {
		jdbcTemplate.update("DELETE FROM Email WHERE correo=?", correo);
	}

	@Override
	void doAction(String sql, Email obj) {
		jdbcTemplate.update(sql, obj.getCorreo(), obj.getEtiqueta(), obj.getId());
	}

	@Override
	public List<String> getAllEtiquetas() {
		try {
            return jdbcTemplate.query("SELECT DISTINCT(etiqueta) FROM Email", new StringRowMapper("etiqueta"));
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<String>();
        }
	}

}
