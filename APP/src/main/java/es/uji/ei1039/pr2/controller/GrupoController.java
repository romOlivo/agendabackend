package es.uji.ei1039.pr2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.uji.ei1039.pr2.dao.GrupoDao;
import es.uji.ei1039.pr2.model.Grupo;

@RestController
@RequestMapping("/grupo")
public class GrupoController {
	
	private GrupoDao grupoDao;
	
	@Autowired
	public void setGrupoDaoo(GrupoDao grupoDao) {
		this.grupoDao = grupoDao; 
	}
	
	@GetMapping("")
	public List<Grupo> getAll(){
		return grupoDao.getAll();
	}
	
	@PostMapping("")
	public String add(@RequestBody Grupo grupo){
		grupoDao.add(grupo);
		return "ok";
	}
	
	@PostMapping("/{nombre}")
	public String delete(@PathVariable String nombre) {
		grupoDao.delete(nombre);
		return "ok";
	}
	

}
