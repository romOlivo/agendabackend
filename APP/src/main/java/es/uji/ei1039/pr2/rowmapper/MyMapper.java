package es.uji.ei1039.pr2.rowmapper;

import org.springframework.jdbc.core.RowMapper;

public interface MyMapper<T> extends RowMapper<T>{
	
	MyMapper<T> create();
	
}
