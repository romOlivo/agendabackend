package es.uji.ei1039.pr2.jdbc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;

import es.uji.ei1039.pr2.dao.ContGrupoDao;
import es.uji.ei1039.pr2.model.ContGrupo;
import es.uji.ei1039.pr2.model.Contacto;
import es.uji.ei1039.pr2.rowmapper.ContGrupoRowMapper;
import es.uji.ei1039.pr2.rowmapper.ContactoRowMapper;

public class ContGrupoJdbc extends JdbcTemplateAbs<ContGrupo> implements ContGrupoDao{
	
	private static final String TABLE_NAME = "ContGrupo";
	private static final int NUM_PARAM = 2;
	
	public ContGrupoJdbc() {
		super(TABLE_NAME, NUM_PARAM, new ContGrupoRowMapper());
	}

	// Añade ua nueva relación
	@Override
	public void add(int id, String nombre) {
		ContGrupo cg = new ContGrupo();
		cg.setId(id);
		cg.setNombre(nombre);
		super.add(cg);
	}

	// Borra una relación
	@Override
	public void delete(int id, String nombre) {
		jdbcTemplate.update("DELETE FROM ContGrupo WHERE id=? AND nombre=?", id, nombre);
	}

	// Devuelve todos los grupos de un contacto
	@Override
	public List<ContGrupo> getAllContacto(int id) {
		try {
            return jdbcTemplate.query("SELECT * FROM ContGrupo WHERE id=?", new ContGrupoRowMapper(), id);
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<ContGrupo>();
        }
	}

	// Devuelve todos los contactos de un grupo
	@Override
	public List<Contacto> getAllGrupo(String grupo) {
		try {
            return jdbcTemplate.query("SELECT c.id, c.nombre, c.apellido FROM ContGrupo cg JOIN Contacto c USING(id) WHERE cg.nombre=?", new ContactoRowMapper(), grupo);
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<Contacto>();
        }
	}

	@Override
	void doAction(String sql, ContGrupo obj) {
		jdbcTemplate.update(sql, obj.getNombre(), obj.getId());
	}

}
