package es.uji.ei1039.pr2.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import es.uji.ei1039.pr2.rowmapper.MyMapper;
import es.uji.ei1039.pr2.utils.SqlGenerator;

public abstract class JdbcTemplateAbs<T> {

	String ADD_SQL;
	String GET_ALL_SQL;
	MyMapper<T> rowMapper;
	
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public JdbcTemplateAbs(String tableName, int numParams, MyMapper<T> rowMapper){
		ADD_SQL = SqlGenerator.generateAddSql(tableName, numParams);
		GET_ALL_SQL = SqlGenerator.generateGetAllSql(tableName);
		this.rowMapper = rowMapper;
	}
	
	abstract void doAction(String sql, T obj);
	
	public void add(T obj) {
		doAction(ADD_SQL, obj);
	}
	
	public List<T> getAll() {
		try {
            return jdbcTemplate.query(GET_ALL_SQL, rowMapper.create());
        }
        catch(EmptyResultDataAccessException e) {
            return new ArrayList<T>();
        }
	}
	
}
