package es.uji.ei1039.pr2.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.uji.ei1039.pr2.model.Grupo;

public class GrupoRowMapper implements MyMapper<Grupo>{

	@Override
	public Grupo mapRow(ResultSet arg0, int arg1) throws SQLException {
		Grupo dev = new Grupo();
		dev.setNombre(arg0.getString("nombre"));
		return dev;
	}

	@Override
	public MyMapper<Grupo> create() {
		return new GrupoRowMapper();
	}

}
