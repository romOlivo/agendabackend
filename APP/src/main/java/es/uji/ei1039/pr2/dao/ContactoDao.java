package es.uji.ei1039.pr2.dao;

import java.util.List;

import es.uji.ei1039.pr2.model.Contacto;

public interface ContactoDao {
	
	int addC(Contacto contacto);
	Contacto get(int id);
	List<Contacto> getAll();
	void delete (Contacto contacto);
	void delete (int id);
	void update (Contacto contacto);
	
}
