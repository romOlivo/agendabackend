package es.uji.ei1039.pr2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.uji.ei1039.pr2.dao.EmailDao;
import es.uji.ei1039.pr2.model.Email;

@RestController
@RequestMapping("/email")
public class EmailController {
	
	private EmailDao emailDao;
	
	@Autowired
	public void setGrupoDaoo(EmailDao emailDao) {
		this.emailDao = emailDao; 
	}
	
	// Devuelve todos los emails de la base de datos
	@GetMapping("")
	public List<Email> getAll(){
		return emailDao.getAll();
	}
	
	// Crea un nuevo email
	@PostMapping("")
	public String add(@RequestBody Email email) {
		emailDao.add(email);
		return "ok";
	}
	
	// Devuelve todos los correos de un contacto
	@GetMapping("/{id}")
	public List<Email> getAllContacto(@PathVariable int id) {
		return emailDao.getAllContacto(id);
	}
	
	// Elimina un email
	@PostMapping("/{correo}")
	public String delete(@PathVariable String correo) {
		emailDao.delete(correo);
		return "ok";
	}
	
	// Devuelve un email concreto
	@GetMapping("/one/{correo}")
	public Email getOne(@PathVariable String correo){
		return emailDao.get(correo);
	}
	
	@GetMapping("/etq")
	public List<String> getAllEtiquetas() {
		return emailDao.getAllEtiquetas();
	}

}
