package es.uji.ei1039.pr2;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import es.uji.ei1039.pr2.dao.ContGrupoDao;
import es.uji.ei1039.pr2.dao.ContactoDao;
import es.uji.ei1039.pr2.dao.EmailDao;
import es.uji.ei1039.pr2.dao.GrupoDao;
import es.uji.ei1039.pr2.dao.TelefonoDao;
import es.uji.ei1039.pr2.jdbc.ContGrupoJdbc;
import es.uji.ei1039.pr2.jdbc.ContactoJdbc;
import es.uji.ei1039.pr2.jdbc.EmailJdbc;
import es.uji.ei1039.pr2.jdbc.GrupoJdbc;
import es.uji.ei1039.pr2.jdbc.TelefonoJdbc;

@Configuration
public class AppConfig {
	
	@Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
	}
	
	@Bean
    public ContactoDao getContactoDao() {
        return new ContactoJdbc();
    }
	
	@Bean
    public TelefonoDao getTelefonoDao() {
        return new TelefonoJdbc();
    }
	
	@Bean
    public GrupoDao getGrupoDao() {
        return new GrupoJdbc();
    }
	
	@Bean
    public EmailDao getEmailDao() {
        return new EmailJdbc();
    }
	
	@Bean
    public ContGrupoDao getContGrupoDao() {
        return new ContGrupoJdbc();
    }

}
