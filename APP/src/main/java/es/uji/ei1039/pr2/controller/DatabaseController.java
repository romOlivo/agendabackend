package es.uji.ei1039.pr2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.uji.ei1039.pr2.dao.ContactoDao;
import es.uji.ei1039.pr2.dao.TelefonoDao;
import es.uji.ei1039.pr2.model.Database;

@RestController
@RequestMapping("/database")
public class DatabaseController {
	
	private ContactoDao contactoDao;
	private TelefonoDao telefonoDao;
	
	@Autowired
	public void setContactoDao(ContactoDao contactoDao) {
		this.contactoDao = contactoDao; 
	}
	
	@Autowired
	public void setTelefonoDao(TelefonoDao telefonoDao) {
		this.telefonoDao = telefonoDao; 
	}
	
	@GetMapping("")
	public Database get(){
		Database database = new Database();
		
		database.setContactos(contactoDao.getAll());
		database.setTelefonos(telefonoDao.getAll());
		
		return database;
	}

}
