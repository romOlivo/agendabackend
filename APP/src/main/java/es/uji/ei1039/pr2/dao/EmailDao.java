package es.uji.ei1039.pr2.dao;

import java.util.List;

import es.uji.ei1039.pr2.model.Email;

public interface EmailDao {
	
	void add (Email email);
	Email get (String correo);
	List<Email> getAllContacto (int id);
	List<Email> getAll();
	void delete(String correo);
	List<String> getAllEtiquetas();

}
