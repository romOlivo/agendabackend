package es.uji.ei1039.pr2.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.uji.ei1039.pr2.model.Email;

public class EmailRowMapper implements MyMapper<Email>{

	@Override
	public Email mapRow(ResultSet arg0, int arg1) throws SQLException {
		Email email = new Email();
		email.setCorreo(arg0.getString("correo"));
		email.setEtiqueta(arg0.getString("etiqueta"));
		email.setId(arg0.getInt("id"));
		return email;
	}

	@Override
	public MyMapper<Email> create() {
		return new EmailRowMapper();
	}

}
