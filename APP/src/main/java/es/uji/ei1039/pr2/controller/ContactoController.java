package es.uji.ei1039.pr2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.uji.ei1039.pr2.dao.ContactoDao;
import es.uji.ei1039.pr2.model.Contacto;

@RestController
@RequestMapping("/contacto")
public class ContactoController {
	
	private ContactoDao contactoDao;
	
	@Autowired
	public void setContactoDao(ContactoDao contactoDao) {
		this.contactoDao = contactoDao; 
	}
	
	@GetMapping("")
	public List<Contacto> get(){
		return contactoDao.getAll();
	}
	
	@PostMapping("")
	public int add(@RequestBody Contacto contacto){
		return contactoDao.addC(contacto);
	}
	
	@PostMapping("/{id}")
	public String delete(@PathVariable int id) {
		contactoDao.delete(id);
		return "ok";
	}
	
	@GetMapping("/{id}")
	public Contacto one(@PathVariable int id) {
		return contactoDao.get(id);
	}
	
	@PostMapping("/upd")
	public String update(@RequestBody Contacto contacto) {
		contactoDao.update(contacto);
		return "ok";
	}
	

}
