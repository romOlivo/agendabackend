package es.uji.ei1039.pr2.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.uji.ei1039.pr2.model.ContGrupo;

public class ContGrupoRowMapper implements MyMapper<ContGrupo>{

	@Override
	public ContGrupo mapRow(ResultSet arg0, int arg1) throws SQLException {
		ContGrupo cg = new ContGrupo();
		cg.setId(arg0.getInt("id"));
		cg.setNombre(arg0.getString("nombre"));
		return cg;
	}

	@Override
	public MyMapper<ContGrupo> create() {
		return new ContGrupoRowMapper();
	}

}
