package es.uji.ei1039.pr2.utils;

public class SqlGenerator {
	
	public static String generateAddSql(String table, int numParams){
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO ");
		sb.append(table);
		sb.append(" VALUES ");
		sb.append(generateStringParamas(numParams));
		sb.append(";");
		return sb.toString();
	}
	
	public static String generateGetAllSql(String table){
		return "SELECT * FROM " + table + ";";
	}
	
	private static String generateStringParamas(int numParams){
		StringBuffer sb = new StringBuffer();
		sb.append("(?");
		for (int i = 1; i < numParams; i++){
			sb.append(", ?");
		}
		sb.append(")");
		return sb.toString();
	}

}
