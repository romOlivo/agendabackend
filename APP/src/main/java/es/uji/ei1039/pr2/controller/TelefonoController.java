package es.uji.ei1039.pr2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.uji.ei1039.pr2.dao.TelefonoDao;
import es.uji.ei1039.pr2.model.Telefono;

@RestController
@RequestMapping("/telefono")
public class TelefonoController {

	private TelefonoDao telefonoDao;
	
	@Autowired
	public void setTelefonoDao(TelefonoDao telefonoDao) {
		this.telefonoDao = telefonoDao; 
	}
	
	@GetMapping("/{id}")
	public List<Telefono> get(@PathVariable int id){
		return telefonoDao.getContactPhones(id);
	}
	
	@PostMapping("")
	public String add(@RequestBody Telefono telefono){
		telefonoDao.add(telefono);
		return "ok";
	}
	
	@PostMapping("/{numero}")
	public String delete(@PathVariable int numero) {
		telefonoDao.delete(numero);
		return "ok";
	}
	
	@GetMapping("/one/{number}")
	public Telefono one(@PathVariable int number) {
		return telefonoDao.get(number);
	}
	
	@GetMapping("/etq")
	public List<String> getAllEtiquetas() {
		return telefonoDao.getAllEtiquetas();
	}
	
	@GetMapping("/all")
	public List<Telefono> getAll(){
		return telefonoDao.getAll();
	}
}
	
	
