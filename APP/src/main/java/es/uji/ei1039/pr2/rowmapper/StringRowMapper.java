package es.uji.ei1039.pr2.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StringRowMapper implements RowMapper<String> {
	
	private String parameterName;
	
	public StringRowMapper(String parameterName){
		this.parameterName = parameterName;
	}

	@Override
	public String mapRow(ResultSet arg0, int arg1) throws SQLException {
		return arg0.getString(parameterName);
	}

}
